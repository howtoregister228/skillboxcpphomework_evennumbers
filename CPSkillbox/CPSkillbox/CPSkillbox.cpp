﻿

#include <iostream>
void oddNum(int finalNum, bool isEven);

int main()
{      
    int finalNum{};
    bool isEven{};
    std::cout << "Enter N: ";
    std::cin >> finalNum;
    std::cout << std::endl;
    std::cout << "Is N even? (0/1):  ";
    std::cin >> isEven;
    std::cout << std::endl;

    oddNum(finalNum, isEven);
}

void oddNum(int finalNum, bool isEven) {
    if (isEven) {

        for (int i = 0; i <= finalNum; i+=2) {
           
                std::cout << i << std::endl;
            
        }
    }
    else
    {
        for (int i = 1; i <= finalNum; i+=2) {
            
                std::cout << i << std::endl;
            
        }
    }
     
   
}

